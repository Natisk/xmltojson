class ConvertionController < ActionController::Base

  def fetch_response
    uri = URI("http://127.0.0.1:3333")
    res = Net::HTTP.get_response(uri)

    render json: {api_response: tranform_data(res.body)}
  end

  def tranform_data(xml_text)
    hash_response = Hash.from_xml(xml_text)
    optional_hash = {'param_1' => 'some_string', 'param_2' => 'some_date', 'some_code' => 'iso_code', 'some_price_param' => 'price'}
    examples_array = Array.new

    hash_response['examples'].each do |elem|
      updated_hash = Hash.new

      elem.each do |k,v|
        if optional_hash.key? k
          updated_hash[optional_hash[k]] = v
        else
          updated_hash[k]= v
        end
      end
      examples_array << updated_hash
    end

    examples_array
  end

end