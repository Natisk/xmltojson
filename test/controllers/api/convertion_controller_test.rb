require 'test_helper'

class ConvertionControllerTest < ActionController::TestCase

  test 'should fetch response' do
    get :fetch_response
    assert_response :success
  end

  test 'should transform response from xml to json' do
    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<examples type=\"array\">\n  <example>\n    <id type=\"integer\">1</id>\n    <name>a</name>\n    <param-1>a</param-1>\n    <param-2 type=\"dateTime\">2013-05-20T00:00:00Z</param-2>\n    <some-code>b</some-code>\n    <some-price-param type=\"float\">1.1</some-price-param>\n  </example>\n  </examples>\n"
    json = '[{"id":1,"name":"a","some_string":"a","some_date":"2013-05-20T00:00:00.000Z","iso_code":"b","price":1.1}]'

    response = ConvertionController.new.tranform_data(xml)
    assert_equal(json, response.to_json)
  end

end

